const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

function validateBook(book) {
  const requiredProperties = ['author', 'name', 'price'];
  for (const prop of requiredProperties) {
    if (!book.hasOwnProperty(prop)) {
      console.error(`Error: Missing property "${prop}" in book`, book);
      return false;
    }
  }
  return true;
}

function createBookList(books) {
  const rootDiv = document.getElementById('root');
  const ulElement = document.createElement('ul');

  books.forEach(book => {
    if (validateBook(book)) {
      const liElement = document.createElement('li');
      liElement.textContent = `Автор: ${book.author}, Назва: ${book.name}, Ціна: ${book.price} грн`;
      ulElement.appendChild(liElement);
    }
  });

  rootDiv.appendChild(ulElement);
}

createBookList(books);
